#ifndef _WIRE
#define _WIRE

#define MU_0 (4*M_PI*1.e-7)

/*

Copyright (C) 2016 Giovanni Organtini <giovanni.organtini@roma1.infn.it>

This file is part of GOMagFieldSim.

GOMagFieldSim is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOMagFieldSim is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOMagFieldSim.  If not, see <http://www.gnu.org/licenses/>.

*/

class Wire {
 protected:
  float _x;
  float _y;
  float _I;
  void init() {
    _x = _y = 0.;
    _I = 1.;
  }
  float r(float x, float y);
  float r2(float x, float y);
 public:
  Wire();
  Wire(float x, float y);
  Wire(float x, float y, float I);
  void setPosition(float x, float y);
  void setCurrent(float I);
  float x() const { return _x; }
  float y() const { return _y; }
  float I() const { return _I; }
  float getCurrent() const { return _I; }
  float fieldAt(float x, float y);
  float fieldAt(float x, float y, int component);
};
#endif
