#include <Wire.h>
#include <math.h>
#include <iostream>

#define LIMIT .01

/*

Copyright (C) 2016 Giovanni Organtini <giovanni.organtini@roma1.infn.it>

This file is part of GOMagFieldSim.

GOMagFieldSim is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOMagFieldSim is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOMagFieldSim.  If not, see <http://www.gnu.org/licenses/>.

*/

int main() {
  float x = -LIMIT;
  float y = -LIMIT;
  float dx = LIMIT/10;
  float dy = LIMIT/10;
  Wire w;
  float max = 0;
  float min = 1.e32;
  while (x < LIMIT) {
    while (y < LIMIT) {
      float Bmod = w.fieldAt(x, y) * 1e6;
      if (max < fabs(Bmod)) {
	max = fabs(Bmod);
      }
      if (min > fabs(Bmod)) {
	min = fabs(Bmod);
      }
      std::cout << x << " " << y << " " << Bmod << std::endl;
      y += dy;
    }
    y = -LIMIT;
    x += dx;
    std::cout << std::endl;
  }
  std::cout << "# " << min << " - " << max << std::endl;
}
