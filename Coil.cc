#include <Coil.h>
#include <math.h>

#include <iostream>

/*

Copyright (C) 2016 Giovanni Organtini <giovanni.organtini@roma1.infn.it>

This file is part of GOMagFieldSim.

GOMagFieldSim is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOMagFieldSim is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOMagFieldSim.  If not, see <http://www.gnu.org/licenses/>.

*/

Coil::Coil() : Wire() { }

Coil::Coil(float r) : Wire() {
  setRadius(r);
}

Coil::Coil(float x, float y, float r) : Wire(x, y) {
  setRadius(r);
}

Coil::Coil(float x, float y, float r, float I) : Wire(x, y, I) {
  setRadius(r);
}

void Coil::setRadius(float r) {
  _r = r;
}

float Coil::fieldAt(float x, float y) {
  float Bx = fieldAt(x, y, 0);
  float By = fieldAt(x, y, 1);
  return sqrt(Bx*Bx + By*By);
}

float Coil::fieldAt(float x, float y, int component) {
  // field from wire 1
  _x += _r;
  float B1x = -MU_0*_I/(2*M_PI*r2(x,y))*(y-_y);
  float B1y =  MU_0*_I/(2*M_PI*r2(x,y))*(x-_x);
  // field from wire 2
  _x -= 2*_r;
  float B2x =  MU_0*_I/(2*M_PI*r2(x,y))*(y-_y);
  float B2y = -MU_0*_I/(2*M_PI*r2(x,y))*(x-_x);
  // back to position
  _x += _r;
  float B = B1x + B2x;
  if (component == 1) {
    B = B1y + B2y;
  }
  return B;
}
