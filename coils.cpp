#include <Coil.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <vector>

#define LIMIT .01
#define NCOILS 10
#define R 0.005
#define DY 0.001;

/*

Copyright (C) 2016 Giovanni Organtini <giovanni.organtini@roma1.infn.it>

This file is part of GOMagFieldSim.

GOMagFieldSim is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOMagFieldSim is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOMagFieldSim.  If not, see <http://www.gnu.org/licenses/>.

*/

void dump(char *fname, float h) {
  char imgname[255];
  sprintf(imgname, "%s", fname);
  imgname[14] = 'p';
  imgname[15] = 'n';
  imgname[16] = 'g';
  std::ofstream gp("gnuplot");
  gp << "set pm3d map" << std::endl;
  gp << "set zrange [1:300]" << std::endl;
  gp << "set term png" << std::endl;
  gp << "set output '" << imgname << "'" << std::endl;
  gp << "splot '" << fname << "' using 1:2:3" << std::endl;
  if (h < 0.012) {
    gp << "unset key" << std::endl;
    gp << "set output 'L" << imgname << "'" << std::endl;
    gp << "plot [-0.01:0.01] [-0.012:0.] '" << fname << "' using 1:2:($4*$3*2*$6):($5*$3*2*$6) with vectors" 
       << std::endl; 
  }
  gp << "quit" << std::endl;
  system("gnuplot gnuplot");
}

int simulate(int nCoils, int frame) {
  std::vector<Coil*> w;
  w.reserve(nCoils + 1);
  float h = 3*LIMIT;
  float yp = -0.010;
  char fname[255];
  while (h > yp) {
    float x = -LIMIT;
    float y = -3*LIMIT;
    float dx = LIMIT/50;
    float dy = LIMIT/50;
    yp = -0.010;
    for (int i = 0; i < nCoils; i++) {
      w[i] = new Coil(0, yp, R);
      yp += DY;
    }
    w[nCoils] = new Coil(0, h, R);
    w[nCoils]->setPosition(0, h);
    float max = 0;
    float min = 1.e32;
    sprintf(fname, "BMapCoilh%04d.dat", frame++);
    std::ofstream of(fname);
    int cline = 0;
    while (x < LIMIT) {
      while (y < 3*LIMIT) {
	int weigth = 0;
	float Bx = 0.;
	float By = 0.;
	for (int i = 0; i < nCoils + 1; i++) {
	  Bx += w[i]->fieldAt(x, y, 0);
	  By += w[i]->fieldAt(x, y, 1);
	}
	float Bmod = sqrt(Bx*Bx + By*By) * 1e6;
	if (max < fabs(Bmod)) {
	  max = fabs(Bmod);
	}
	if (min > fabs(Bmod)) {
	  min = fabs(Bmod);
	}
	if (cline % 5 == 0) {
	  weigth = 1;
	}
	of << x << " " << y << " " << Bmod << " "
	   << Bx/Bmod << " " << By/Bmod << " " << weigth << std::endl;
	cline++;
	y += dy;
      }
      y = -3*LIMIT;
      x += dx;
      of << std::endl;
    }
    of << "# " << min << " - " << max << std::endl;
    of.close();
    dump(fname, h);
    h -= DY;
  }
  return frame;
}

int main(int argc, char **argv) {
  int frame = 0;
  for (int i = 1; i < 10; i++) {
    frame = simulate(i, frame);
  }
}
