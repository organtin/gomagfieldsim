#include <Wire.h>
#include <math.h>

#define MU_0 (4*M_PI*1.e-7) 

/*

Copyright (C) 2016 Giovanni Organtini <giovanni.organtini@roma1.infn.it>

This file is part of GOMagFieldSim.

GOMagFieldSim is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOMagFieldSim is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOMagFieldSim.  If not, see <http://www.gnu.org/licenses/>.

*/

Wire::Wire() {
  init();
}

Wire::Wire(float x, float y) {
  init();
  setPosition(x, y);
}

Wire::Wire(float x, float y, float I) {
  setPosition(x, y);
  setCurrent(I);
}

float Wire::r(float x, float y) {
  return sqrt(r2(x, y));
}

float Wire::r2(float x, float y) {
  return (x-_x)*(x-_x) + (y-_y)*(y-_y);
}

void Wire::setPosition(float x, float y) {
  _x = x;
  _y = y;
}

void Wire::setCurrent(float I) {
  _I = I;
}

float Wire::fieldAt(float x, float y) {
  return MU_0*_I/(2*M_PI*r(x, y));
}

float Wire::fieldAt(float x, float y, int component) {
  float rr = -(y-_y)/r(x, y);
  if (component == 1) {
    rr = (x-_x)/r(x, y);
  }
  return fieldAt(x, y)*rr;
}
