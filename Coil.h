#ifndef _COIL
#define _COIL

#include <Wire.h>

/*

Copyright (C) 2016 Giovanni Organtini <giovanni.organtini@roma1.infn.it>

This file is part of GOMagFieldSim.

GOMagFieldSim is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GOMagFieldSim is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GOMagFieldSim.  If not, see <http://www.gnu.org/licenses/>.

*/

class Coil: public Wire {
 protected:
  float _r;
 public:
  Coil();
  Coil(float r);
  Coil(float x, float y, float r);
  Coil(float x, float y, float r, float I);
  void setRadius(float r);
  float getRadius() const { return _r; };
  float fieldAt(float x, float y);
  float fieldAt(float x, float y, int component);
};

#endif
